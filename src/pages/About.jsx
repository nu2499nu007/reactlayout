import React from "react";
import MainLayout from "../components/layouts/MainLayout";

const About = () => {
  return (
    <MainLayout>
      <h1>About Page</h1>
      <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error,
        adipisci cupiditate magnam, culpa ipsam quidem inventore natus vitae ut
        omnis aliquid eaque, aspernatur nesciunt architecto fugiat voluptatem
        tempore quisquam ratione.
      </p>
    </MainLayout>
  );
};

export default About;
