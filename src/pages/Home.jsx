import React from "react";
import MainLayout from "../components/layouts/MainLayout";

const Home = () => {
  return (
    <MainLayout>
      <h1>Home Page</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci eius
        veniam ea sed nostrum non, consequuntur, cum id voluptates iste, ratione
        eligendi consectetur molestias a libero! Facilis accusamus enim nobis.
      </p>
    </MainLayout>
  );
};

export default Home;
