import React from "react";
import CartItem from "./CartItem";
const product = [
  {
    imgUrl:
      "https://img.kingpowerclick.com/cdn-cgi/image/format=auto/kingpower-com/image/upload/w_640,h_640,f_auto/v1606360559/prod/8295198-L1.jpg",
    name: "IPHONE 12",
    price: 29900.0,
    qty: 2,
  },
  {
    imgUrl:
      "https://img.kingpowerclick.com/cdn-cgi/image/format=auto/kingpower-com/image/upload/w_640,h_640,f_auto/v1617856036/prod/8303020-L1.jpg",
    name: "XIAOMI Redmi 9",
    price: 3990.0,
    qty: 2,
  },
  {
    imgUrl:
      "https://img.kingpowerclick.com/cdn-cgi/image/format=auto/kingpower-com/image/upload/w_640,h_640,f_auto/v1587977390/prod/8218121-L1.jpg",
    name: "HUAWEI P40 (Blush Gold)",
    price: 20990,
    qty: 2,
  },
];
const Cart = () => {
  return (
    <div className="container">
      <div className="row">
        {/* ทำการวนลูป */}
        {product.map((product, index) => {
          return (
            <div className="col-md-4 my-3" key={index}>
              {/* <CartItem
                imgurl={product.imgUrl}
                name={product.name}
                price={product.price}
                qty={product.qty}
              /> */}
              <CartItem product={product} />
            </div>
          );
        })}
      </div>
      {/* <CartItem name="Banana" price="30" qty="2"></CartItem>
      <CartItem name="Apple" price="25" qty="3"></CartItem> */}
    </div>
  );
};

export default Cart;
