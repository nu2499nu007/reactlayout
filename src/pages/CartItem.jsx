/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";

const CartItem = (props) => {
  console.log(props);
  //การทำ Destructuring
  const { imgUrl, name, price, qty } = props.product;
  return (
    // <div className="card">
    //   <p>Name : {props.name}</p>
    //   <p>Price : {props.price}</p>
    //   <p>QTY : {props.qty}</p>
    // </div>

    <div className="card" style={{ width: "18rem" }}>
      <img src={imgUrl} className="card-img-top" alt="" />
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <p className="card-text">{price}</p>
        <p className="card-text">{qty} </p>
      </div>
    </div>
  );
};

export default CartItem;
