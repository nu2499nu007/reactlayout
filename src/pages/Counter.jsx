import React, { useState, useEffect } from "react";

const Counter = () => {
  //   let counter = 0;
  //   const showCounter = () => {
  //     counter = counter + 1;
  //     console.log(counter);
  //   };
  //การใช้งาน State
  const [counter, setCounter] = useState(0);
  //การใช้ UseEffect
  useEffect(() => {
      //สั่ง setค่า
    setCounter(50);
  }, []);
  return (
    <div>
      <h1>Counter : {counter}</h1>
      <button onClick={() => setCounter(counter + 1)}>Counter up</button>
      <button onClick={() => setCounter(counter - 1)}>Counter Down</button>
    </div>
  );
};

export default Counter;
