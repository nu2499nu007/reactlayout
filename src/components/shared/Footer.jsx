const Footer = () => {
  return (
    <footer>
      <div className="bg-dark py-4 text-white">
        <div className="container">
          <div className="col-md-6">&copy;CopyRight 2020-2020</div>
          <div className="col-md-6 text-end">www.creditonline.com</div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
